package com.example.AppBar.Service;

import java.util.List;
import java.util.Optional;

import com.example.AppBar.Entity.User;

public interface UserService {
    User saveUser(User user);
    Optional<User> getUserById(Long id);
    List<User> getAllUsers();
    User updateUser(Long id, User user);
    void deleteUser(Long id);
    Optional<User> findByUsername(String username);
}
