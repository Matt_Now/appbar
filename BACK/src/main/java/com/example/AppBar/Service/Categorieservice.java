package com.example.AppBar.Service;


import java.util.List;

import org.springframework.stereotype.Service;

import com.example.AppBar.Entity.Category;

@Service
public interface Categorieservice {
    public Category addCategory(Category Category);
    public Category getCategoryById(long id);
    public List<Category> getCategories();
    public void deleteCategory(long id);
    public Category updateCategory(Category Category);
    boolean existsById(long id);
}