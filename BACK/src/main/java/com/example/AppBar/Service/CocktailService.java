package com.example.AppBar.Service;

import java.util.List;
import org.springframework.stereotype.Service;
import com.example.AppBar.Entity.Cocktail;

@Service
public interface CocktailService {
    Cocktail addCocktail(Cocktail cocktail);
    Cocktail getCocktailById(long id);
    List<Cocktail> getCocktails();
    void deleteCocktail(long id);
    Cocktail updateCocktail(Cocktail cocktail);
    List<Cocktail> getCocktailsByCategory(Long categoryId);
}
