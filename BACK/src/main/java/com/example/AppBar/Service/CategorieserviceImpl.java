package com.example.AppBar.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.AppBar.Entity.Category;
import com.example.AppBar.Repository.CategoryRepository;

@Service
public class CategorieserviceImpl implements Categorieservice {

    @Autowired
    private CategoryRepository CategoryRepository;

    @Override
    public Category addCategory(Category Category) {
        return CategoryRepository.save(Category);
    }

    @Override
    public Category getCategoryById(long id) {
        return CategoryRepository.findById(id).get();
    }

    @Override
    public List<Category> getCategories() {
        return (List<Category>) CategoryRepository.findAll();
    }

    @Override
    public void deleteCategory(long id) {
        CategoryRepository.deleteById(id);
    }

    @Override
    public Category updateCategory(Category Category) {
        return CategoryRepository.save(Category);
    }

    @Override
    public boolean existsById(long id) {
        return CategoryRepository.existsById(id);
    }

}
