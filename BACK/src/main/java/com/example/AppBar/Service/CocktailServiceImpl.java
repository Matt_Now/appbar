package com.example.AppBar.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.AppBar.Entity.Cocktail;
import com.example.AppBar.Repository.CocktailRepository;

@Service
public class CocktailServiceImpl implements CocktailService {

    @Autowired
    private CocktailRepository cocktailRepository;

    @Override
    public Cocktail addCocktail(Cocktail cocktail) {
        return cocktailRepository.save(cocktail);
    }

    @Override
    public Cocktail getCocktailById(long id) {
        return cocktailRepository.findById(id).get();
    }

    @Override
    public List<Cocktail> getCocktails() {
        return (List<Cocktail>) cocktailRepository.findAll();
    }

    @Override
    public void deleteCocktail(long id) {
        cocktailRepository.deleteById(id);
    }

    @Override
    public Cocktail updateCocktail(Cocktail cocktail) {
        return cocktailRepository.save(cocktail);
    }

    public List<Cocktail> getCocktailsByCategory(Long categoryId) {
        return cocktailRepository.findByCategoryId(categoryId);
    }
}
