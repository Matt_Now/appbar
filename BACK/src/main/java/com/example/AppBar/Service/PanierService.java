package com.example.AppBar.Service;

import com.example.AppBar.Entity.Panier;
import com.example.AppBar.Entity.Cocktail;
import com.example.AppBar.Repository.PanierRepository;
import com.example.AppBar.Repository.CocktailDansPanierRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class PanierService {

    @Autowired
    private PanierRepository panierRepository;

    @Autowired
    private CocktailDansPanierRepository cocktailDansPanierRepository;

    public Panier creerPanier(String nomCommande) {
        Panier panier = new Panier();
        panier.setNomCommande(nomCommande);
        panier.setEtatAvancement("commencé");
        return panierRepository.save(panier);
    }

    public Optional<Panier> obtenirPanier(Long id) {
        return panierRepository.findById(id);
    }

    public void ajouterCocktailAuPanier(Long panierId, Cocktail cocktail, int quantite, String taille) {
        Optional<Panier> panierOpt = panierRepository.findById(panierId);
        if (panierOpt.isPresent()) {
            Panier panier = panierOpt.get();
            panier.ajouterCocktail(cocktail, quantite, taille);
            panierRepository.save(panier);
        }
    }

    public void retirerCocktailDuPanier(Long panierId, Cocktail cocktail) {
        Optional<Panier> panierOpt = panierRepository.findById(panierId);
        if (panierOpt.isPresent()) {
            Panier panier = panierOpt.get();
            panier.retirerCocktail(cocktail);
            panierRepository.save(panier);
        }
    }

    public double calculerPrixTotal(Long panierId) {
        Optional<Panier> panierOpt = panierRepository.findById(panierId);
        return panierOpt.map(Panier::calculerPrixTotal).orElse(0.0);
    }
    
    public List<Panier> obtenirTousLesPaniers() {
        return panierRepository.findAll();
    }

    public Panier mettreAJourEtatPanier(Long id, String etatAvancement) {
        Optional<Panier> optionalPanier = obtenirPanier(id);
        if (optionalPanier.isPresent()) {
            Panier panier = optionalPanier.get();
            panier.setEtatAvancement(etatAvancement);
            return panierRepository.save(panier);  // Utilisez directement panierRepository.save()
        } else {
            throw new RuntimeException("Panier non trouvé");
        }
    }
}
