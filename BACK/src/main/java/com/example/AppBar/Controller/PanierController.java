package com.example.AppBar.Controller;

import com.example.AppBar.Entity.Cocktail;
import com.example.AppBar.Entity.Panier;
import com.example.AppBar.Service.PanierService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/panier")
public class PanierController {

    @Autowired
    private PanierService panierService;

    @PostMapping
    public ResponseEntity<Panier> creerPanier(@RequestBody Map<String, Object> payload) {
        String nomCommande = (String) payload.get("nomCommande");
        List<Map<String, Object>> items = (List<Map<String, Object>>) payload.get("items");

        Panier panier = panierService.creerPanier(nomCommande);
        for (Map<String, Object> item : items) {
            Long cocktailId = ((Number) ((Map<String, Object>) item.get("cocktail")).get("id")).longValue();
            int quantite = ((Number) item.get("quantite")).intValue();
            String taille = (String) item.get("taille");

            Cocktail cocktail = new Cocktail(); 
            cocktail.setId(cocktailId);
            panierService.ajouterCocktailAuPanier(panier.getId(), cocktail, quantite, taille);
        }

        return ResponseEntity.ok(panier);
    }

    @GetMapping("/{id}")
    public Optional<Panier> obtenirPanier(@PathVariable Long id) {
        return panierService.obtenirPanier(id);
    }

    @GetMapping
    public List<Map<String, Object>> obtenirTousLesPaniers() {
        return panierService.obtenirTousLesPaniers().stream().map(panier -> {
            Map<String, Object> data = new HashMap<>();
            data.put("id", panier.getId());  // Ajouter l'ID du panier ici
            data.put("nomCommande", panier.getNomCommande());
            data.put("etatAvancement", panier.getEtatAvancement());
            return data;
        }).collect(Collectors.toList());
    }

    @PostMapping("/{id}/ajouter-cocktail")
    public void ajouterCocktail(@PathVariable Long id, @RequestParam Long cocktailId, @RequestParam int quantite, @RequestParam String taille) {
        Cocktail cocktail = new Cocktail();
        cocktail.setId(cocktailId);
        panierService.ajouterCocktailAuPanier(id, cocktail, quantite, taille);
    }

    @DeleteMapping("/{id}/retirer-cocktail")
    public void retirerCocktail(@PathVariable Long id, @RequestParam Long cocktailId) {
        Cocktail cocktail = new Cocktail(); 
        cocktail.setId(cocktailId);
        panierService.retirerCocktailDuPanier(id, cocktail);
    }

    @GetMapping("/{id}/prix-total")
    public double calculerPrixTotal(@PathVariable Long id) {
        return panierService.calculerPrixTotal(id);
    }

    // Ajouter la méthode PUT pour mettre à jour l'état du panier
    @PutMapping("/{id}")
    public ResponseEntity<Panier> mettreAJourEtatPanier(@PathVariable Long id, @RequestBody Map<String, String> payload) {
        String etatAvancement = payload.get("etatAvancement");
        Panier panier = panierService.mettreAJourEtatPanier(id, etatAvancement);
        return ResponseEntity.ok(panier);
    }
}
