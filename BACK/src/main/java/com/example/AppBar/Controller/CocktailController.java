package com.example.AppBar.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.UrlResource;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;

import com.example.AppBar.Entity.Category;
import com.example.AppBar.Entity.Cocktail;
import com.example.AppBar.Service.Categorieservice;
import com.example.AppBar.Service.CocktailService;

import java.util.List;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

@RestController
@CrossOrigin(origins = "http://localhost:5173")
public class CocktailController {

    @Autowired
    private CocktailService cocktailService;

    @Autowired
    private Categorieservice categorieservice;

    private static final String UPLOAD_DIR = "./uploads/";

    @RequestMapping("/")
    public String hello_world() {
        return "Hello World";
    }

    @PostMapping("/cocktail/add")
    public ResponseEntity<String> addCocktail(@RequestParam("libelle") String libelle,
                                              @RequestParam("description") String description,
                                              @RequestParam("image") MultipartFile image,
                                              @RequestParam("categoryId") Long categoryId,
                                              @RequestParam("basePrice") double basePrice, // Ajout de basePrice comme paramètre requis
                                              @RequestParam(required = false) Double mediumPrice,
                                              @RequestParam(required = false) Double largePrice) {
        try {
            String filename = saveImage(image);
            if (filename == null) {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Image upload failed");
            }
    
            Category category = categorieservice.getCategoryById(categoryId);
            if (category == null) {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Invalid category ID");
            }
    
            Cocktail cocktail = new Cocktail();
            cocktail.setLibelle(libelle);
            cocktail.setDescription(description);
            cocktail.setImageUrl(filename);
            cocktail.setCategory(category);
            cocktail.setBasePrice(basePrice); 
            cocktail.setMediumPrice(mediumPrice); 
            cocktail.setLargePrice(largePrice);   
            cocktailService.addCocktail(cocktail);
    
            return ResponseEntity.ok("Cocktail Added Successfully with Image.");
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Failed to add cocktail: " + e.getMessage());
        }
    }
    
    


    private String saveImage(MultipartFile file) throws IOException {
        if (file.isEmpty()) {
            return null;
        }
        File uploadDir = new File(UPLOAD_DIR);
        if (!uploadDir.exists()) uploadDir.mkdirs();

        String originalFilename = file.getOriginalFilename();
        String filename = System.currentTimeMillis() + "_" + originalFilename;
        Path targetLocation = Paths.get(UPLOAD_DIR + filename);

        Files.copy(file.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);

        return filename;
    }

    @GetMapping("/images/{filename:.+}")
    @ResponseBody
    public ResponseEntity<Resource> serveFile(@PathVariable String filename) {
        try {
            Path file = Paths.get(UPLOAD_DIR).resolve(filename);
            Resource resource = new UrlResource(file.toUri());
            if (resource.exists() || resource.isReadable()) {
                return ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION,
                        "attachment; filename=\"" + resource.getFilename() + "\"").body(resource);
            } else {
                throw new RuntimeException("Could not read the file!");
            }
        } catch (MalformedURLException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        }
    }

    // getCocktailById
    @RequestMapping("/cocktail/{id}")
    public Cocktail getCocktailById(@PathVariable("id") long id) {
        return cocktailService.getCocktailById(id);
    }

    // getCocktails
    @RequestMapping("/cocktails")
    public List<Cocktail> getCocktails() {
        return cocktailService.getCocktails();
    }

    // updateCocktail
    @PutMapping("/cocktail")
    public Cocktail updateCocktail(@RequestBody Cocktail cocktail) {
        return cocktailService.updateCocktail(cocktail);
    }

    // deleteCocktailById
    @DeleteMapping("/cocktail/{id}")
    public String deleteCocktail(@PathVariable("id") long id) {
        cocktailService.deleteCocktail(id);
        return "Cocktail Deleted";
    }

    @GetMapping("/cocktails/category/{categoryId}")
    public ResponseEntity<List<Cocktail>> getCocktailsByCategory(@PathVariable Long categoryId) {
        try {
            List<Cocktail> cocktails = cocktailService.getCocktailsByCategory(categoryId);
            if (cocktails.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
            return new ResponseEntity<>(cocktails, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
