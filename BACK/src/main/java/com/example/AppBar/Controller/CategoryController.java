package com.example.AppBar.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.AppBar.Entity.Category;
import com.example.AppBar.Service.Categorieservice;

@RestController
@CrossOrigin(origins = "http://localhost:5173")
@RequestMapping("/categories") 
public class CategoryController {

    @Autowired
    private Categorieservice Categorieservice;

    @RequestMapping("/hello")
    public String hello_world(){
        return "Hello World";
    }

    // add Category
    @PostMapping("/add")
    public String addCategory(@RequestBody Category category){
        Categorieservice.addCategory(category);
        return "Category Added Successfully..";
    }

    // getCategoryById
    @RequestMapping("/{id}")
    public Category getCategoryById(@PathVariable("id") long id){
        return Categorieservice.getCategoryById(id);
    }

    // getCategories
    @RequestMapping
    public List<Category> getCategories(){
        return Categorieservice.getCategories();
    }

    // updateCategory
    @PutMapping
    public Category updateCategory(@RequestBody Category category){
        return Categorieservice.updateCategory(category);
    }

    // deleteCategoryById
    @DeleteMapping("/{id}")
    public String deleteCategory(@PathVariable("id") long id){
        Categorieservice.deleteCategory(id);
        return "Category Deleted";
    }
}
