package com.example.AppBar.Repository;

import com.example.AppBar.Entity.CocktailDansPanier;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CocktailDansPanierRepository extends JpaRepository<CocktailDansPanier, Long> {
}