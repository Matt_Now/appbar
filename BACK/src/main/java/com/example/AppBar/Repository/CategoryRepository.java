package com.example.AppBar.Repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.example.AppBar.Entity.Category;

@Repository
public interface CategoryRepository extends CrudRepository<Category, Long> {
}