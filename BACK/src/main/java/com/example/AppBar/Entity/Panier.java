package com.example.AppBar.Entity;

import java.util.ArrayList;
import java.util.List;
import jakarta.persistence.*;

@Entity
@Table(name = "panier")
public class Panier {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToMany(mappedBy = "panier", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<CocktailDansPanier> cocktails = new ArrayList<>();

    private String etatAvancement;
    private String nomCommande;

    // Constructeurs
    public Panier() {
    }

    // Ajouter un cocktail au panier
    public void ajouterCocktail(Cocktail cocktail, int quantite, String taille) {
        CocktailDansPanier cocktailDansPanier = new CocktailDansPanier(this, cocktail, quantite, taille);
        cocktails.add(cocktailDansPanier);
    }
    
    // Retirer un cocktail du panier
    public void retirerCocktail(Cocktail cocktail) {
        cocktails.removeIf(cp -> cp.getCocktail().equals(cocktail));
    }

    // Calculer le prix total du panier
    public double calculerPrixTotal() {
        double total = 0;
        for (CocktailDansPanier cp : cocktails) {
            total += cp.getQuantite() * cp.getCocktail().getPriceBySize(cp.getTaille());
        }
        return total;
    }    

    // Getters et Setters
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<CocktailDansPanier> getCocktails() {
        return cocktails;
    }

    public void setCocktails(List<CocktailDansPanier> cocktails) {
        this.cocktails = cocktails;
    }

    public String getEtatAvancement() {
        return etatAvancement;
    }

    public void setEtatAvancement(String etatAvancement) {
        this.etatAvancement = etatAvancement;
    }

    public String getNomCommande() {
        return nomCommande;
    }

    public void setNomCommande(String nomCommande) {
        this.nomCommande = nomCommande;
    }
}
