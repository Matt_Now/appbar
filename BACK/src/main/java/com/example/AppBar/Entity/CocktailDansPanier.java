package com.example.AppBar.Entity;

import java.io.Serializable;

import jakarta.persistence.*;

@Entity
@Table(name = "cocktail_dans_panier")
public class CocktailDansPanier {
    @EmbeddedId
    private CocktailDansPanierId id = new CocktailDansPanierId();

    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId("panierId")
    private Panier panier;

    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId("cocktailId")
    private Cocktail cocktail;

    private int quantite;
    private String taille; 

    // Constructeurs
    public CocktailDansPanier() {
    }

    public CocktailDansPanier(Panier panier, Cocktail cocktail, int quantite, String taille) {
        this.panier = panier;
        this.cocktail = cocktail;
        this.quantite = quantite;
        this.taille = taille;
        this.id = new CocktailDansPanierId(panier.getId(), cocktail.getId());
    }

    // Getters et Setters
    public Panier getPanier() {
        return panier;
    }

    public void setPanier(Panier panier) {
        this.panier = panier;
    }

    public Cocktail getCocktail() {
        return cocktail;
    }

    public void setCocktail(Cocktail cocktail) {
        this.cocktail = cocktail;
    }

    public int getQuantite() {
        return quantite;
    }

    public void setQuantite(int quantite) {
        this.quantite = quantite;
    }

    public String getTaille() {
        return taille;
    }

    public void setTaille(String taille) {
        this.taille = taille;
    }
}

@Embeddable
class CocktailDansPanierId implements Serializable {
    @Column(name = "panier_id")
    private Long panierId;

    @Column(name = "cocktail_id")
    private Long cocktailId;

    // Constructeurs, getters et setters
    public CocktailDansPanierId() {
    }

    public CocktailDansPanierId(Long panierId, Long cocktailId) {
        this.panierId = panierId;
        this.cocktailId = cocktailId;
    }

}
