package com.example.AppBar;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AppBarApplication {
    public static void main(String[] args) {
        SpringApplication.run(AppBarApplication.class, args);
    }
}
