# AppBar



## Pour commencer

Pour commencer assurez vous d'avoir java installé et node. Un serveur SQL aussi.

Personnellement j'utilise PHP MY ADMIN https://www.apachefriends.org/fr/index.html
VS CODE JAVA  https://code.visualstudio.com/docs/languages/java

et pour node https://nodejs.org/en/download/package-manager

et si vous voulez lancer le projet via docker : https://www.docker.com/products/docker-desktop/

/!\ Il faudra bien configuré la bdd dans le docker-compose.yml et dans \BACK\src\main\resources\application.properties et ne pas avoir le même port ouvert /!\ 

* A cause d'un problème de magento (utiliser pour mon travail) j'ai tout qui a cassé, j'ai néanmoins tenu à faire mon maximum pour le docker du projet * 


## Après avoir tout installé 

Après avoir tout installé pour devait ouvrir les deux dossier avec VSCODE ou autre editeur de code.

Pour Java simplement vous lancer avec le picto lancer 

Pour Vue js vous faite : npm i puis npm run dev pour installer et dépendances et lancer le serveur. 

Pour docker vous faites : docker-compose up --build

## Plus qu'à vous lancer dans les cocktails 

Après que tout soit bien lancé vous pouvez maintenant vous servir du site.

Il faudra créer un user dans la base de donnée et vous connecter en front avec l'url /connexion 

ensuite pour ajouter des cocktails vous faites cocktail/add

## Merci de m'avoir lu et j'espère vous aurez aimé ce projet

