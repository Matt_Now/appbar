import { createApp } from 'vue';
import App from './App.vue';
import router from './router';
import Axios from 'axios';
import store from '../store'; 
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/js/bootstrap.bundle.min.js';

const app = createApp(App);

// Configurer Axios comme propriété globale
app.config.globalProperties.$axios = Axios;

app.use(router);
app.use(store); // Utilisez le store Vuex

app.mount('#app');
