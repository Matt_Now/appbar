// router/index.js
import { createRouter, createWebHistory } from 'vue-router';
import HomePage from '../views/HomePage.vue';
import ViewCocktails from '../views/ViewCocktails.vue';
import Panier from '../views/Panier.vue';
import Commandes from '@/views/Commandes.vue';
import Login from '@/views/Login.vue';
import BackOffice from '@/views/BackOffice.vue';
import CommandesManage from '@/views/CommandesManage.vue';

const routes = [
  {
    path: '/',
    name: 'home',
    component: HomePage
  },
  {
    path: '/cocktail',
    name: 'cocktails',
    component: ViewCocktails
  },
  {
    path: '/cocktail/add',  
    name: 'addcocktail',
    component: () => import('../views/AddCocktail.vue')
  },
  {
    path: '/edit/:id',
    name: 'edit',
    component: () => import('../views/UpdateCocktail.vue')
  },
  {
    path: '/carte',
    name: 'carte',
    component: () => import('../views/Carte.vue')
  },
  {
    path: '/cocktail/:id',
    name: 'viewCocktail',
    component: () => import('../views/ViewCocktail.vue'),
    props: true
  },
  {
    path: '/panier',
    name: 'Panier',
    component: Panier
  },
  {
    path: '/commandes',
    name: 'Commandes',
    component: Commandes
  },
  {
    path: '/connexion',
    name: 'Login',
    component: Login
  },
  {
    path: '/backoffice',
    name: 'BackOffice',
    component: BackOffice
  },
  {
    path: '/commandes/manage',
    name: 'CommandesManage',
    component: CommandesManage
  }
];

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes
});

export default router;
